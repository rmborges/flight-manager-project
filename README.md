# Flight Manager Project


This project was developed as a backend API that provides flights
information between Oporto -> Lisbon or Lisbon -> Oporto for the
companies TAP (TP) and Ryanair (FR). The API can provide all flights
available for those companies or filter by city origin and destination,
currency or even using a range of dates for the departure date. It can
also provide a calculation for the average price of all flights with
same logic of filters. It also records the requests that are executed,
provides those records and allows to delete them.

**Stack:**
* Storage data in MongoDB (viewer in: http://localhost:27017/);
* Mongo Express to view data from MongoDB (viewer in: http://localhost:8089/);
* Backend using Java 13 with Spring Boot framework;
* Docker to manage java artifacts and database image;
* Environment file with ports, names of components and credentials;
* Usage of cache for faster responses;
* Usage of logs for traceability;
* Tests to check compliance;



## Run application

Clone/download repository go to root directory where we can find docker-compose file. 

Then run `docker-compose up` to start application. 
 
Navigate to `http://localhost:8080/` to interact with this API (default
port but can be configured in the env file).
 
To stop application run `docker-compose down`. Data won't be lost.
 
If want to clean also data from database run `docker-compose down -v`
 
Enjoy!

## API Usage

Possible requests:

1. get all flights (`/flights`)
   *  Example: `http://localhost:8080/flights?flyFrom=OPO&to=LIS&partner=picky&curr=GBP&date_from=04/02/2020&date_to=05/02/2020`

| Parameter | Description                 | Mandatory |
|:----------|:----------------------------|:----------|
| flyFrom   | origin  city code           | x         |
| to        | destination  city code      |           |
| curr      | currency   currency code    |           |
| date_from | range start day/month/year  |           |
| date_to   | range finish day/month/year |           |

2. get average flight data (`/flights/avg`)
   *  Example: `http://localhost:8080/flights/avg?flyFrom=OPO&to=LIS&curr=GBP&date_from=04/02/2020&date_to=05/02/2020`

3. get dummy/default flights from source info (`/getSkypickerFlights`)
   *  Example: `http://localhost:8080/getSkypickerFlights`

4. get all records of requests (`/audit`)
   *  Example: `http://localhost:8080/audit`

5. delete all records of requests (`/audit`)
   *  Example: `http://localhost:8080/audit`

Example to view database data:

![pic](README/0.png)

Example of getting flights with filters:

![pic](README/1.png)

Example of getting flight average with filters:

![pic](README/2.png)

Example of getting recorded requests:

![pic](README/3.png)