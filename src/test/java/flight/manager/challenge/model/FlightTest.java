package flight.manager.challenge.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class FlightTest {

    private Flight flight;
    private HashMap bags = new HashMap<>();
    private HashMap priceCurrency = new HashMap<>();



    @BeforeEach
    void setUp() {
        flight = new Flight();
        bags.put(1,45.5);
        bags.put(2,91);
        priceCurrency.put("GBP", 20);
        priceCurrency.put("EUR", 23);
    }

    @AfterEach
    void tearDown() {
        flight = new Flight();
        bags.put(1,null);
        bags.put(2,null);
        priceCurrency.put("GBP", null);
        priceCurrency.put("EUR", null);
    }

    @Test
    void getFlyFrom() {
        //Given
        flight = new Flight();

        //Then
        assertNull(flight.getFlyFrom());
    }

    @Test
    void setFlyFrom() {
        //Given
        flight = new Flight();
        String expected = "OPO";

        //When
        flight.setFlyFrom("OPO");

        //Then
        assertEquals(flight.getFlyFrom(), expected);
    }

    @Test
    void getFlyTo() {
    }

    @Test
    void setFlyTo() {
    }

    @Test
    void getCityFrom() {
    }

    @Test
    void setCityFrom() {
    }

    @Test
    void getCityTo() {
    }

    @Test
    void setCityTo() {
    }

    @Test
    void getAirlines() {
    }

    @Test
    void setAirlines() {
    }

    @Test
    void getBags_price() {
    }

    @Test
    void setBags_price() {
    }

    @Test
    void getPrice() {
    }

    @Test
    void setPrice() {
    }

    @Test
    void getConversion() {
    }

    @Test
    void setConversion() {
    }

    @Test
    void getdTimeUTC() {
    }

    @Test
    void setdTimeUTC() {
    }

    @Test
    void getaTimeUTC() {
    }

    @Test
    void setaTimeUTC() {
    }

}