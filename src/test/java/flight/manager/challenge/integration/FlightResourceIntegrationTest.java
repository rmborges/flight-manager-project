package flight.manager.challenge.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import flight.manager.challenge.dto.FlightDTO;
import flight.manager.challenge.dto.RequestDTO;
import flight.manager.challenge.exception.FlightAPIException;
import flight.manager.challenge.exception.FlightNotAcceptableException;
import flight.manager.challenge.exception.FlightNotFoundException;
import flight.manager.challenge.model.Request;
import flight.manager.challenge.repository.RequestRepository;
import flight.manager.challenge.service.AuditService;
import flight.manager.challenge.service.FlightService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integrationtests")
class FlightResourceIntegrationTest {

    //bind RANDOM_PORT
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private FlightService flightService;
    @Autowired
    private AuditService auditService;
    @Autowired
    private RequestRepository requestRepository;


    private MultiValueMap<String, String> params;
    private Map<Object, Object> queryParams;

    private final List<FlightDTO> flightDTOList = new ArrayList<>();
    private final FlightDTO flightDTO = new FlightDTO();
    private final RequestDTO requestDTO = new RequestDTO();
    private final Request request = new Request();


    @BeforeEach
    void setUp() throws FlightNotFoundException, FlightAPIException, JsonProcessingException {

        this.queryParams = new HashMap<>();
        this.queryParams.put("flyFrom", "OPO");
        this.queryParams.put("to", "LIS");
        this.queryParams.put("partner", "picky");
        this.queryParams.put("curr", "GBP");

    }


    @AfterEach
    void tearDown() {
        this.queryParams = new HashMap<>();

    }

    @Test
    void getFlights_should_return_200_ok() throws MalformedURLException {

        // Given parameters for search, queryParams
        final UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("http://localhost:" + this.port + "/flights")
                .queryParam("flyFrom", "OPO")
                .queryParam("to", "LIS")
                .queryParam("partner", "picky")
                .queryParam("curr", "GBP");

        // When executing search
        final ResponseEntity<?> response = this.restTemplate.getForEntity(builder.toUriString(), String.class);

        // Then
        assertNotNull(response.getBody());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getFlights_should_return_406_not_acceptable() throws FlightNotAcceptableException {

        // Given parameters for search, queryParams without mandatory field
        final UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("http://localhost:" + this.port + "/flights")
//                .queryParam("flyFrom", "OPO")
                .queryParam("to", "LIS")
                .queryParam("partner", "picky")
                .queryParam("curr", "GBP");

        // When executing search
        final ResponseEntity<?> response = this.restTemplate.getForEntity(builder.toUriString(), String.class);

        // Then
        assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode());

    }
}