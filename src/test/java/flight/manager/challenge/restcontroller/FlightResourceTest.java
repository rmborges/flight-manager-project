package flight.manager.challenge.restcontroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import flight.manager.challenge.dto.FlightDTO;
import flight.manager.challenge.dto.RequestDTO;
import flight.manager.challenge.exception.FlightAPIException;
import flight.manager.challenge.exception.FlightNotAcceptableException;
import flight.manager.challenge.exception.FlightNotFoundException;
import flight.manager.challenge.model.Request;
import flight.manager.challenge.service.AuditService;
import flight.manager.challenge.service.FlightService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest()
class FlightResourceTest {


    @Mock
    private FlightService flightService;
    @Mock
    private AuditService auditService;
    //    @Mock
//    private RequestRepository requestRepository;
    @InjectMocks
    private FlightResource flightResource;

    private MockMvc mvc;
    private JacksonTester<List<FlightDTO>> jacksonDto;


    private Map<Object, Object> queryParams;

    private final List<FlightDTO> flightDTOList = new ArrayList<>();
    private final FlightDTO flightDTO = new FlightDTO();
    private final RequestDTO requestDTO = new RequestDTO();
    private final Request request = new Request();


    @BeforeEach
    void setUp() {

        this.queryParams = new HashMap<>();
        this.queryParams.put("flyFrom", "OPO");
        this.queryParams.put("to", "LIS");
        this.queryParams.put("partner", "picky");
        this.queryParams.put("curr", "GBP");

        this.flightDTOList.add(this.flightDTO);

    }

    @BeforeEach
    void setMockService() {
        mvc = MockMvcBuilders.standaloneSetup(flightResource).build();

        JacksonTester.initFields(this, new ObjectMapper());

        when(this.auditService.addRequest(this.requestDTO)).thenReturn(this.request);
//        when(requestRepository.save(request)).thenReturn(request);

    }

    @AfterEach
    void tearDown() {
        this.queryParams = new HashMap<>();
    }

    @Test
    void controller_Initialized_Correctly() {
        assertNotNull(this.flightResource);
    }

    @Test
    void getFlights_should_return_200_ok() throws IOException, FlightAPIException, FlightNotAcceptableException, FlightNotFoundException {
        // Given parameters for search, queryParams
        when(this.flightService.getFlights(this.queryParams)).thenReturn(this.flightDTOList);

        // When executing search
        final ResponseEntity<?> response = this.flightResource.getFlights(this.queryParams);

        // Then
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getFlights_should_return_406_not_acceptable() throws IOException, FlightAPIException, FlightNotAcceptableException, FlightNotFoundException {

        // Given parameters for search, queryParams empty
        when(this.flightService.getFlights(any(Map.class))).thenThrow(new FlightNotAcceptableException("invalid params"));

        // When executing search
        final ResponseEntity<?> response = this.flightResource.getFlights(this.queryParams);

        // Then
        assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode());
    }

    @Test
    void getFlights_should_return_200_ok_json_body() throws Exception {
        // Given parameters for search, queryParams
        when(this.flightService.getFlights(any(Map.class))).thenReturn(this.flightDTOList);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("/flights")
                .queryParam("flyFrom", "OPO")
                .queryParam("to", "LIS")
                .queryParam("partner", "picky")
                .queryParam("curr", "GBP");

        // When executing search, expects a list of flightDTO as json
        mvc.perform(get(builder.toUriString()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(jacksonDto.write(flightDTOList).getJson()));
    }
}