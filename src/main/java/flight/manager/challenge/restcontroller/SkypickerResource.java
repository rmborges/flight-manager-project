package flight.manager.challenge.restcontroller;

import com.fasterxml.jackson.core.JsonProcessingException;
import flight.manager.challenge.service.SkypickerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The dummy Skypicker resource, gets all data with default url.
 */
@RestController
@RequestMapping("getSkypickerFlights")
public class SkypickerResource {

    /**
     * The Skypicker service.
     */
    @Autowired
    SkypickerService skypickerService;

    /**
     * Gets flights.
     *
     * @return the flights
     * @throws JsonProcessingException the json processing exception
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getFlights() throws JsonProcessingException {

        return this.skypickerService.getFlights();

    }
}
