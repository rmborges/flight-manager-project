package flight.manager.challenge.restcontroller;

import com.fasterxml.jackson.core.JsonProcessingException;
import flight.manager.challenge.dto.RequestDTO;
import flight.manager.challenge.model.Request;
import flight.manager.challenge.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * The Audit resource for records.
 */
@RestController
@RequestMapping("audit")
public class AuditResource {

    /**
     * The Audit service.
     */
    @Autowired
    AuditService auditService;

    /**
     * Gets all requests.
     *
     * @return the all requests
     * @throws JsonProcessingException the json processing exception
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllRequests() throws JsonProcessingException {


        return ResponseEntity.status(HttpStatus.OK).body(this.auditService.getAllRequests());

    }

    /**
     * Add request response entity.
     *
     * @param requestDTO the request dto
     * @return the response entity
     * @throws JsonProcessingException the json processing exception
     */
    @PostMapping(value = "")
    public ResponseEntity<?> addRequest(@RequestBody final RequestDTO requestDTO) throws JsonProcessingException {

        final Request request = this.auditService.addRequest(requestDTO);


        return request != null ? ResponseEntity.status(HttpStatus.OK).body(requestDTO) : ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("");

    }

    /**
     * Delete all requests response entity.
     *
     * @return the response entity
     * @throws JsonProcessingException the json processing exception
     */
    @DeleteMapping(value = "")
    public ResponseEntity<?> deleteAllRequests() throws JsonProcessingException {

        this.auditService.deleteAllRequests();
        return ResponseEntity.status(HttpStatus.OK).body("");

    }
}
