package flight.manager.challenge.restcontroller;

import flight.manager.challenge.dto.FlightAverageDTO;
import flight.manager.challenge.dto.FlightDTO;
import flight.manager.challenge.dto.RequestDTO;
import flight.manager.challenge.exception.FlightAPIException;
import flight.manager.challenge.exception.FlightNotAcceptableException;
import flight.manager.challenge.exception.FlightNotFoundException;
import flight.manager.challenge.service.AuditService;
import flight.manager.challenge.service.FlightService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The Flight resource.
 */
@RestController
@RequestMapping("flights")
public class FlightResource {

    private static final String PARTNER_KEY = "partner";
    private static final String PARTNER_VALUE = "picky";
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    /**
     * The Flight service.
     */
    @Autowired
    FlightService flightService;

    /**
     * The Audit service.
     */
    @Autowired
    AuditService auditService;

    /**
     * Gets flights.
     *
     * @param queryParams the query params
     * @return the flights
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getFlights(
            @RequestParam() final Map<Object, Object> queryParams
    ) {

        queryParams.putIfAbsent(PARTNER_KEY, PARTNER_VALUE);

        final TreeMap<Object, Object> sortedMap = new TreeMap<>(queryParams);

        final RequestDTO request = new RequestDTO();
        request.setRequestParams(queryParams);

        ResponseEntity<?> response;

        try {
            final List<FlightDTO> flights = this.flightService.getFlights(sortedMap);
            response = ResponseEntity.ok().body(flights);

        } catch (final FlightNotAcceptableException e) {
            this.LOG.error(e.getLocalizedMessage(), e);
            response = new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
        } catch (final FlightNotFoundException e) {
            this.LOG.error(e.getLocalizedMessage(), e);
            response = new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
        } catch (final flight.manager.challenge.exception.FlightAPIException | java.io.IOException e) {
            this.LOG.error(e.getLocalizedMessage(), e);
            response = new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
        request.setResponseStatus(response.getStatusCode().toString());
        this.auditService.addRequest(request);

        return response;

    }

    /**
     * Gets avg flights.
     *
     * @param queryParams the query params
     * @return the avg flights
     */
    @GetMapping(value = "avg", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAvgFlights(
            @RequestParam() final Map<Object, Object> queryParams
    ) {

        queryParams.putIfAbsent(PARTNER_KEY, PARTNER_VALUE);

        final TreeMap<Object, Object> sortedMap = new TreeMap<>(queryParams);

        final RequestDTO request = new RequestDTO();
        request.setRequestParams(queryParams);

        ResponseEntity<?> response;
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        try {
            final FlightAverageDTO flightDTO = this.flightService.getAverageFlight(sortedMap);
            response = ResponseEntity.ok().headers(headers).body(flightDTO);

        } catch (final FlightNotAcceptableException e) {
            this.LOG.error(e.getLocalizedMessage(), e);
            response = new ResponseEntity<>(e, HttpStatus.NOT_ACCEPTABLE);
        } catch (final FlightNotFoundException e) {
            this.LOG.error(e.getLocalizedMessage(), e);
            response = new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
        } catch (final FlightAPIException | IOException e) {
            this.LOG.error(e.getLocalizedMessage(), e);
            response = new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }

        request.setResponseStatus(response.getStatusCode().toString());
        this.auditService.addRequest(request);

        return response;

    }
}
