package flight.manager.challenge.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Generic exception.
 */
public class GenericException extends Exception {


    private static final long serialVersionUID = 100L;
    private static final String DEFAULT_MESSAGE = "Error {0}";
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());
    private final List<Object> parameters = new ArrayList<>();
    private final String resourceCode;
    private final String defaultMessage;


    /**
     * Instantiates a new Generic exception.
     *
     * @param resourceCode the resource code
     */
    public GenericException(final String resourceCode) {
        this.resourceCode = resourceCode;
        this.defaultMessage = DEFAULT_MESSAGE;
    }

    /**
     * Instantiates a new Generic exception.
     *
     * @param resourceCode the resource code
     * @param message      the message
     */
    public GenericException(final String resourceCode, final String message) {
        super(message);
        this.resourceCode = resourceCode;
        this.defaultMessage = message;

    }

    /**
     * Instantiates a new Generic exception.
     *
     * @param resourceCode   the resource code
     * @param defaultMessage the default message
     * @param message        the message
     */
    public GenericException(final String resourceCode, final String defaultMessage, final String message) {
        super(message);
        this.resourceCode = resourceCode;
        this.defaultMessage = defaultMessage;
    }

    /**
     * Instantiates a new Generic exception.
     *
     * @param message        the message
     * @param cause          the cause
     * @param resourceCode   the resource code
     * @param defaultMessage the default message
     */
    public GenericException(final String message, final Throwable cause, final String resourceCode, final String defaultMessage) {
        super(message, cause);
        this.resourceCode = resourceCode;
        this.defaultMessage = defaultMessage;
    }

    /**
     * Instantiates a new Generic exception.
     *
     * @param cause          the cause
     * @param resourceCode   the resource code
     * @param defaultMessage the default message
     */
    public GenericException(final Throwable cause, final String resourceCode, final String defaultMessage) {
        super(cause);
        this.resourceCode = resourceCode;
        this.defaultMessage = defaultMessage;
    }

    /**
     * Instantiates a new Generic exception.
     *
     * @param message            the message
     * @param cause              the cause
     * @param enableSuppression  the enable suppression
     * @param writableStackTrace the writable stack trace
     * @param resourceCode       the resource code
     * @param defaultMessage     the default message
     */
    public GenericException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace, final String resourceCode, final String defaultMessage) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.resourceCode = resourceCode;
        this.defaultMessage = defaultMessage;
    }

    /**
     * Gets parameters.
     *
     * @return the parameters
     */
    public List<Object> getParameters() {
        return this.parameters;
    }

    /**
     * Gets resource code.
     *
     * @return the resource code
     */
    public String getResourceCode() {
        return this.resourceCode;
    }

    /**
     * Gets default message.
     *
     * @return the default message
     */
    public String getDefaultMessage() {
        return this.defaultMessage;
    }

    /**
     * Add parameters.
     *
     * @param parameter the parameter
     */
    protected void addParameters(final Object parameter) {
        this.parameters.add(parameter);
    }

    @Override
    public String getLocalizedMessage() {
        return MessageFormat.format("{0} {1}", this.resourceCode, (this.parameters.isEmpty()) ? this.defaultMessage : MessageFormat.format(this.defaultMessage, this.parameters.toArray()));
    }
}
