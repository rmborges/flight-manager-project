package flight.manager.challenge.exception;

import flight.manager.challenge.utils.ErrorCodes;

import java.util.Map;

/**
 * The type Flight not acceptable exception.
 */
public class FlightNotAcceptableException extends GenericException {

    private static final String DEFAULT_MESSAGE_PATTERN = "Error: invalid or missing parameters {0}";

    /**
     * Instantiates a new Flight not acceptable exception.
     *
     * @param resourceCode   the resource code
     * @param defaultMessage the default message
     */
    public FlightNotAcceptableException(final String resourceCode, final String defaultMessage) {
        super(resourceCode, defaultMessage);
    }

    /**
     * Instantiates a new Flight not acceptable exception.
     *
     * @param message the message
     */
    public FlightNotAcceptableException(final String message) {
        super(ErrorCodes.FLIGHT_NOT_ACCEPTABLE.getResourceCode(), message);
    }

    /**
     * Instantiates a new Flight not acceptable exception.
     *
     * @param queryParams the query params
     */
    public FlightNotAcceptableException(final Map<Object, Object> queryParams) {
        super(ErrorCodes.FLIGHT_NOT_ACCEPTABLE.getResourceCode(), DEFAULT_MESSAGE_PATTERN);

        final StringBuilder params = new StringBuilder();
        String prefix = "";
        for (final Map.Entry<Object, Object> entry : queryParams.entrySet()) {
            params.append(prefix);
            prefix = "; ";
            params.append(entry.getKey());
            params.append("=");
            params.append(entry.getValue());
        }
        this.addParameters(params.toString());
    }


}
