package flight.manager.challenge.exception;

import flight.manager.challenge.utils.ErrorCodes;

import java.util.Map;

/**
 * The type Flight api exception.
 */
public class FlightAPIException extends GenericException {

    private static final String DEFAULT_MESSAGE_PATTERN = "Error: failed connection {0}";

    /**
     * Instantiates a new Flight api exception.
     *
     * @param resourceCode   the resource code
     * @param defaultMessage the default message
     */
    public FlightAPIException(final String resourceCode, final String defaultMessage) {
        super(resourceCode, defaultMessage);
    }

    /**
     * Instantiates a new Flight api exception.
     *
     * @param message the message
     */
    public FlightAPIException(final String message) {
        super(ErrorCodes.FLIGHT_API.getResourceCode(), message);
    }

    /**
     * Instantiates a new Flight api exception.
     *
     * @param queryParams the query params
     */
    public FlightAPIException(final Map<Object, Object> queryParams) {
        super(ErrorCodes.FLIGHT_API.getResourceCode(), DEFAULT_MESSAGE_PATTERN);

        final StringBuilder params = new StringBuilder();
        String prefix = "";
        for (final Map.Entry<Object, Object> entry : queryParams.entrySet()) {
            params.append(prefix);
            prefix = "; ";
            params.append(entry.getKey());
            params.append("=");
            params.append(entry.getValue());
        }
        this.addParameters(params.toString());
    }


}
