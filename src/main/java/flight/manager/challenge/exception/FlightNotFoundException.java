package flight.manager.challenge.exception;

import flight.manager.challenge.utils.ErrorCodes;

import java.util.Map;

/**
 * The type Flight not found exception.
 */
public class FlightNotFoundException extends GenericException {

    private static final String DEFAULT_MESSAGE_PATTERN = "Error: Could not find flights for params: {0}";

    /**
     * Instantiates a new Flight not found exception.
     *
     * @param resourceCode   the resource code
     * @param defaultMessage the default message
     */
    public FlightNotFoundException(final String resourceCode, final String defaultMessage) {
        super(resourceCode, defaultMessage);
    }

    /**
     * Instantiates a new Flight not found exception.
     *
     * @param message the message
     */
    public FlightNotFoundException(final String message) {
        super(ErrorCodes.FLIGHT_NOT_FOUND.getResourceCode(), message);
    }

    /**
     * Instantiates a new Flight not found exception.
     *
     * @param queryParams the query params
     */
    public FlightNotFoundException(final Map<Object, Object> queryParams) {
        super(ErrorCodes.FLIGHT_NOT_FOUND.getResourceCode(), DEFAULT_MESSAGE_PATTERN);
        this.addParameters(queryParams.toString());
    }


}
