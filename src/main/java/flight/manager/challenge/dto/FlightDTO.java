package flight.manager.challenge.dto;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The type Flight dto.
 */
public class FlightDTO {
    private String cityFrom;
    private String airportFrom;
    private String cityTo;
    private String airportTo;
    private ArrayList<String> airlines;
    private HashMap bags_price;
    private float price;
    private String currency;
    private String departureDate;
    private String arrivingDate;

    /**
     * Gets city from.
     *
     * @return the city from
     */
    public String getCityFrom() {
        return this.cityFrom;
    }

    /**
     * Sets city from.
     *
     * @param cityFrom the city from
     */
    public void setCityFrom(final String cityFrom) {
        this.cityFrom = cityFrom;
    }

    /**
     * Gets airport from.
     *
     * @return the airport from
     */
    public String getAirportFrom() {
        return this.airportFrom;
    }

    /**
     * Sets airport from.
     *
     * @param airportFrom the airport from
     */
    public void setAirportFrom(final String airportFrom) {
        this.airportFrom = airportFrom;
    }

    /**
     * Gets city to.
     *
     * @return the city to
     */
    public String getCityTo() {
        return this.cityTo;
    }

    /**
     * Sets city to.
     *
     * @param cityTo the city to
     */
    public void setCityTo(final String cityTo) {
        this.cityTo = cityTo;
    }

    /**
     * Gets airport to.
     *
     * @return the airport to
     */
    public String getAirportTo() {
        return this.airportTo;
    }

    /**
     * Sets airport to.
     *
     * @param airportTo the airport to
     */
    public void setAirportTo(final String airportTo) {
        this.airportTo = airportTo;
    }

    /**
     * Gets airlines.
     *
     * @return the airlines
     */
    public ArrayList<String> getAirlines() {
        return this.airlines;
    }

    /**
     * Sets airlines.
     *
     * @param airlines the airlines
     */
    public void setAirlines(final ArrayList<String> airlines) {
        this.airlines = airlines;
    }

    /**
     * Gets bags price.
     *
     * @return the bags price
     */
    public HashMap getBags_price() {
        return this.bags_price;
    }

    /**
     * Sets bags price.
     *
     * @param bags_price the bags price
     */
    public void setBags_price(final HashMap<Object, Object> bags_price) {
        this.bags_price = bags_price;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public float getPrice() {
        return this.price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(final float price) {
        this.price = price;
    }

    /**
     * Gets currency.
     *
     * @return the currency
     */
    public String getCurrency() {
        return this.currency;
    }

    /**
     * Sets currency.
     *
     * @param currency the currency
     */
    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    /**
     * Gets departure date.
     *
     * @return the departure date
     */
    public String getDepartureDate() {
        return this.departureDate;
    }

    /**
     * Sets departure date.
     *
     * @param departureDate the departure date
     */
    public void setDepartureDate(final String departureDate) {
        this.departureDate = departureDate;
    }

    /**
     * Gets arriving date.
     *
     * @return the arriving date
     */
    public String getArrivingDate() {
        return this.arrivingDate;
    }

    /**
     * Sets arriving date.
     *
     * @param arrivingDate the arriving date
     */
    public void setArrivingDate(final String arrivingDate) {
        this.arrivingDate = arrivingDate;
    }
}
