package flight.manager.challenge.dto;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * The type Request dto.
 */
public class RequestDTO {

    private String requestId;
    private LocalDateTime requestDateTime = LocalDateTime.now();
    private Map<Object, Object> requestParams;
    private String responseStatus;

    /**
     * Gets request id.
     *
     * @return the request id
     */
    public String getRequestId() {
        return this.requestId;
    }

    /**
     * Sets request id.
     *
     * @param requestId the request id
     */
    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    /**
     * Gets request date time.
     *
     * @return the request date time
     */
    public LocalDateTime getRequestDateTime() {
        return this.requestDateTime;
    }

    /**
     * Sets request date time.
     *
     * @param requestDateTime the request date time
     */
    public void setRequestDateTime(final LocalDateTime requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    /**
     * Gets request params.
     *
     * @return the request params
     */
    public Map<Object, Object> getRequestParams() {
        return this.requestParams;
    }

    /**
     * Sets request params.
     *
     * @param requestParams the request params
     */
    public void setRequestParams(final Map<Object, Object> requestParams) {
        this.requestParams = requestParams;
    }

    /**
     * Gets response status.
     *
     * @return the response status
     */
    public String getResponseStatus() {
        return this.responseStatus;
    }

    /**
     * Sets response status.
     *
     * @param responseStatus the response status
     */
    public void setResponseStatus(final String responseStatus) {
        this.responseStatus = responseStatus;
    }
}
