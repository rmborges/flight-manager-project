package flight.manager.challenge.dto;

import java.util.HashMap;

/**
 * The type Flight average dto.
 */
public class FlightAverageDTO {
    private String cityFrom;
    private String cityTo;
    private HashMap bags_price;
    private float price;
    private String currency;

    /**
     * Gets city from.
     *
     * @return the city from
     */
    public String getCityFrom() {
        return this.cityFrom;
    }

    /**
     * Sets city from.
     *
     * @param cityFrom the city from
     */
    public void setCityFrom(final String cityFrom) {
        this.cityFrom = cityFrom;
    }

    /**
     * Gets city to.
     *
     * @return the city to
     */
    public String getCityTo() {
        return this.cityTo;
    }

    /**
     * Sets city to.
     *
     * @param cityTo the city to
     */
    public void setCityTo(final String cityTo) {
        this.cityTo = cityTo;
    }

    /**
     * Gets bags price.
     *
     * @return the bags price
     */
    public HashMap getBags_price() {
        return this.bags_price;
    }

    /**
     * Sets bags price.
     *
     * @param bags_price the bags price
     */
    public void setBags_price(final HashMap<Object, Object> bags_price) {
        this.bags_price = bags_price;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public float getPrice() {
        return this.price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(final float price) {
        this.price = price;
    }

    /**
     * Gets currency.
     *
     * @return the currency
     */
    public String getCurrency() {
        return this.currency;
    }

    /**
     * Sets currency.
     *
     * @param currency the currency
     */
    public void setCurrency(final String currency) {
        this.currency = currency;
    }

}
