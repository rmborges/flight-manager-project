package flight.manager.challenge.repository;

import flight.manager.challenge.model.Request;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Request repository.
 */
@Repository
public interface RequestRepository extends MongoRepository<Request, String> {
}
