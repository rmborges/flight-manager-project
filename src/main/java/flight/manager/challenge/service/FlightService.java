package flight.manager.challenge.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import flight.manager.challenge.dto.FlightAverageDTO;
import flight.manager.challenge.dto.FlightDTO;
import flight.manager.challenge.exception.FlightAPIException;
import flight.manager.challenge.exception.FlightNotAcceptableException;
import flight.manager.challenge.exception.FlightNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * The interface Flight service.
 */
@Service
public interface FlightService {

    /**
     * Gets flights.
     *
     * @param queryParams the query params
     * @return the flights
     * @throws IOException                  the io exception
     * @throws FlightNotFoundException      the flight not found exception
     * @throws FlightAPIException           the flight api exception
     * @throws FlightNotAcceptableException the flight not acceptable exception
     */
    List<FlightDTO> getFlights(Map<Object, Object> queryParams) throws IOException, FlightNotFoundException, FlightAPIException, FlightNotAcceptableException;

    /**
     * Gets average flight.
     *
     * @param queryParams the query params
     * @return the average flight
     * @throws FlightAPIException           the flight api exception
     * @throws FlightNotAcceptableException the flight not acceptable exception
     * @throws FlightNotFoundException      the flight not found exception
     * @throws JsonProcessingException      the json processing exception
     */
    FlightAverageDTO getAverageFlight(Map<Object, Object> queryParams) throws FlightAPIException, FlightNotAcceptableException, FlightNotFoundException, JsonProcessingException;

    Map<Object, Object> getFlightsMap(Map<Object, Object> queryParams) throws FlightNotAcceptableException, FlightAPIException, FlightNotFoundException, JsonProcessingException;

    ResponseEntity<String> callRestAPI(String url) throws FlightAPIException;
}
