package flight.manager.challenge.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import flight.manager.challenge.model.Flight;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The type Skypicker service.
 */
@Service
public class SkypickerServiceImpl implements SkypickerService {

    private static final String URL = "https://api.skypicker.com/flights?flyFrom=OPO&to=LIS&partner=picky&curr=GBP";


    @Override
    @Cacheable("skypicker")
    public ResponseEntity<?> getFlights() throws JsonProcessingException {

        final RestTemplate restTemplate = new RestTemplate();
        final ResponseEntity<String> response = restTemplate.getForEntity(URL, String.class);

        if (response.getStatusCode().equals(HttpStatus.OK) && !StringUtils.isEmpty(response.getBody())) {
            final ObjectMapper mapper = new ObjectMapper();
            final JsonNode root = mapper.readTree(response.getBody());
            final JsonNode data = root.path("data");
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
            final List<Flight> flights = mapper.readValue(data.toString(), new TypeReference<>() {
            });

            final List<Flight> flightsTransformed = new ArrayList<>();

            final List<String> airlines = new ArrayList<>(Arrays.asList("FR", "TP"));
            Collections.sort(airlines);

            for (int i = flights.size() - 1; i >= 0; i--) {

                final List<String> flightAirlines = flights.get(i).getAirlines();
                Collections.sort(flightAirlines);

                final List<String> common = new ArrayList<>(flightAirlines);
                common.retainAll(airlines);

                final List<String> different = new ArrayList<>(flightAirlines);
                different.removeAll(airlines);

                // if has any airline and not any other that is not on the search list
                if (!common.isEmpty() && different.isEmpty()) {
                    flightsTransformed.add(flights.get(i));
                }
            }
            return ResponseEntity.status(HttpStatus.OK).body(flightsTransformed);
        }


        return response;
    }
}
