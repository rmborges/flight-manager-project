package flight.manager.challenge.service;

import flight.manager.challenge.dto.RequestDTO;
import flight.manager.challenge.model.Request;
import flight.manager.challenge.repository.RequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The type Audit request service.
 */
@Service
public class AuditRequestServiceImpl implements AuditService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public List<Request> getAllRequests() {
        this.LOG.info("Getting all requests.");
        return this.requestRepository.findAll();
    }

    @Override
    public Request addRequest(final RequestDTO requestDTO) {

        final Request request = new Request();
        request.setRequestParams(requestDTO.getRequestParams());
        request.setResponseStatus(requestDTO.getResponseStatus());
        request.setRequestDateTime(requestDTO.getRequestDateTime());
        this.LOG.info("Adding request: {}.", request.getRequestDateTime());

        return this.requestRepository.save(request);
    }

    @Override
    public void deleteAllRequests() {
        this.LOG.info("Deleting all requests.");
        this.requestRepository.deleteAll();
    }
}
