package flight.manager.challenge.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * The interface Skypicker service.
 */
@Service
public interface SkypickerService {

    /**
     * Gets flights.
     *
     * @return the flights
     * @throws JsonProcessingException the json processing exception
     */
    ResponseEntity<?> getFlights() throws JsonProcessingException;
}
