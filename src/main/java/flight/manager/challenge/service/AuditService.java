package flight.manager.challenge.service;

import flight.manager.challenge.dto.RequestDTO;
import flight.manager.challenge.model.Request;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The interface Audit service.
 */
@Service
public interface AuditService {

    /**
     * Gets all requests.
     *
     * @return the all requests
     */
    List<Request> getAllRequests();

    /**
     * Add request request.
     *
     * @param request the request
     * @return the request
     */
    Request addRequest(RequestDTO request);

    /**
     * Delete all requests.
     */
    void deleteAllRequests();
}
