package flight.manager.challenge.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import flight.manager.challenge.dto.FlightAverageDTO;
import flight.manager.challenge.dto.FlightDTO;
import flight.manager.challenge.exception.FlightAPIException;
import flight.manager.challenge.exception.FlightNotAcceptableException;
import flight.manager.challenge.exception.FlightNotFoundException;
import flight.manager.challenge.model.Flight;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * The type Flight service.
 */
@Service
@CacheConfig(cacheNames = "FlightServiceImpl")
public class FlightServiceImpl implements FlightService {

    private static final String BASE_URL = "https://api.skypicker.com/flights";
    private static final List AIRLINES = List.of("FR", "TP");
    private static final List MANDATORY_PARAMS = List.of("flyFrom", "partner");
    private static final RestTemplate restTemplate = new RestTemplate();
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String FLY_FROM_KEY = "flyFrom";
    private static final String FLY_TO_KEY = "to";
    private static final String CURRENCY_KEY = "curr";
    private static final String FLIGHTS_KEY = "flights";
    private static final String AVG_KEY = "price_average";
    private static final String BAGS_KEY = "bags_price_average";
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired // self-autowired to use cache between methods of the same class
    private FlightService self;

    @Override
//    @Cacheable(value = "getFlights", sync = true)
    public List<FlightDTO> getFlights(final Map<Object, Object> queryParams) throws IOException, FlightNotFoundException, FlightAPIException, FlightNotAcceptableException {
        this.LOG.info("Get flights for : " + queryParams.toString());

        final Map<Object, Object> flightsTransfConcurr = this.self.getFlightsMap(queryParams);
        return (List<FlightDTO>) flightsTransfConcurr.get(FLIGHTS_KEY);

    }

    /**
     * Gets flights map.
     *
     * @param queryParams the query params
     * @return the flights map
     * @throws FlightNotAcceptableException the flight not acceptable exception
     * @throws FlightAPIException           the flight api exception
     * @throws FlightNotFoundException      the flight not found exception
     * @throws JsonProcessingException      the json processing exception
     */
    @Override
    @Cacheable(value = "getFlightsMap", sync = true)
    public Map<Object, Object> getFlightsMap(final Map<Object, Object> queryParams) throws FlightNotAcceptableException, FlightAPIException, FlightNotFoundException, JsonProcessingException {
        this.validateParameters(queryParams);
        final String url = this.buildURL(queryParams);

        final ResponseEntity<String> response = this.self.callRestAPI(url);

        if (!response.getStatusCode().equals(HttpStatus.OK) || StringUtils.isEmpty(response.getBody())) {
            this.LOG.error("Fail on get flights.");
            throw new FlightNotFoundException(queryParams);
        }

        final JsonNode root = mapper.readTree(response.getBody());
        final JsonNode data = root.path("data");
        final JsonNode originalCurrency = root.path("currency");
        final JsonNode originalCurrencyRate = root.path("currency_rate");
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        final List<Flight> flights = mapper.readValue(data.toString(), new TypeReference<>() {
        });

        final Map<Object, Object> flightsOtherParams = new HashMap<>();
        flightsOtherParams.put("currency", mapper.readValue(originalCurrency.toString(), new TypeReference<>() { }));
        flightsOtherParams.put("currency_rate", mapper.readValue(originalCurrencyRate.toString(), new TypeReference<>() { }));
        final CopyOnWriteArrayList<Flight> concurrentList = new CopyOnWriteArrayList<>(flights);

        flightsOtherParams.put("flightsList", concurrentList);


        return this.processFlightsConcurrency(queryParams, flightsOtherParams);
    }

    @Override
//    @Cacheable(value = "getAverageFlight", sync = true)
    public FlightAverageDTO getAverageFlight(final Map<Object, Object> queryParams) throws FlightAPIException, FlightNotAcceptableException, FlightNotFoundException, JsonProcessingException {
        this.LOG.info("Get flight average for : " + queryParams.toString());

        final Map<Object, Object> flightsTransfConcurr = this.self.getFlightsMap(queryParams);

        final FlightAverageDTO flightAverageDTO = new FlightAverageDTO();
        flightAverageDTO.setCityFrom(queryParams.containsKey(FLY_FROM_KEY) ? queryParams.get(FLY_FROM_KEY).toString() : "");
        flightAverageDTO.setCityTo(queryParams.containsKey(FLY_TO_KEY) ? queryParams.get(FLY_TO_KEY).toString() : "");
        flightAverageDTO.setCurrency((String) flightsTransfConcurr.get(CURRENCY_KEY));
        flightAverageDTO.setBags_price((HashMap<Object, Object>) flightsTransfConcurr.get(BAGS_KEY));
        flightAverageDTO.setPrice((Float) flightsTransfConcurr.get(AVG_KEY));

        return flightAverageDTO;
    }

    private Map<Object, Object> processFlightsConcurrency(final Map<Object, Object> queryParams, final Map<Object, Object> flightsData) {

        final List<FlightDTO> flightsTransformed = new ArrayList<>();
        final float price = 0;
        float avgPrice = 0;
        final HashMap<String, Object> bags_price = new HashMap<>();
        String currency = "";
        final CopyOnWriteArrayList<Flight> flights = (CopyOnWriteArrayList<Flight>) flightsData.get("flightsList");
        final Iterator<Flight> iteratorFlights = flights.iterator();
        while (iteratorFlights.hasNext()) {
            final Flight flight = iteratorFlights.next();
            boolean valid = false;

            final List<String> flightAirlines = flight.getAirlines();

            final List<String> common = new ArrayList<>(flightAirlines);
            common.retainAll(AIRLINES);

            final List<String> different = new ArrayList<>(flightAirlines);
            different.removeAll(AIRLINES);

            valid = true;


            // if has any airline and not any other that is not on the search list
            if (!common.isEmpty() && different.isEmpty()) {
                valid = true;
            }

            if (valid) {
                final HashMap<Object, Object> bags = flight.getBags_price();
                String finalCurrency = currency;
                bags.replaceAll((k, v) -> ((double) v) /
                        (finalCurrency.equalsIgnoreCase("EUR") ? 1 :
                                (double) flightsData.get("currency_rate")));


                final FlightDTO transformedFlight = new FlightDTO();
                transformedFlight.setAirlines(flight.getAirlines());
                transformedFlight.setAirportFrom(flight.getFlyFrom());
                transformedFlight.setAirportTo(flight.getFlyTo());
                transformedFlight.setCityFrom(flight.getCityFrom());
                transformedFlight.setCityTo(flight.getCityTo());
                currency = queryParams.containsKey(CURRENCY_KEY) ? queryParams.get(CURRENCY_KEY).toString() :
                        (flight.getConversion().entrySet().iterator().hasNext() ? (String) flight.getConversion().keySet().toArray()[0] : "");
                transformedFlight.setCurrency(currency);
                transformedFlight.setBags_price(bags);
                transformedFlight.setPrice(flight.getPrice());
                transformedFlight.setDepartureDate(Instant.ofEpochSecond(flight.getdTimeUTC()).toString());
                transformedFlight.setArrivingDate(Instant.ofEpochSecond(flight.getaTimeUTC()).toString());

                flightsTransformed.add(transformedFlight);

                avgPrice += flight.getPrice();
                for (final Map.Entry<Object, Object> entry : bags.entrySet()) {
                    final String key = entry.getKey().toString();
                    final var value = (Double) entry.getValue();
                    final var value2 = (Double) bags_price.get(key);
                    if (!bags_price.containsKey(key)) {
                        bags_price.put(key, value);
                    } else {
                        bags_price.put(key, value + value2);
                    }

                }
            }
        }

        Collections.sort(flightsTransformed, Comparator.comparing(FlightDTO::getPrice));

        final var result = new HashMap<>();
        result.put(FLIGHTS_KEY, flightsTransformed);
        result.put(AVG_KEY, avgPrice / flightsTransformed.size());
        bags_price.replaceAll((k, v) -> (double) v / (double) flightsTransformed.size());
        result.put(BAGS_KEY, bags_price);
        result.put(CURRENCY_KEY, currency);

        return result;
    }

    /**
     * Call rest api response entity.
     *
     * @param url the url
     * @return the response entity
     * @throws FlightAPIException the flight api exception
     */
    @Override
    @Cacheable(value = "skypickerFlights", sync = true)
    public ResponseEntity<String> callRestAPI(final String url) throws FlightAPIException {
        final ResponseEntity<String> responseEntity;

        try {
            responseEntity = restTemplate.getForEntity(url, String.class);
        } catch (final Exception e) {
            this.LOG.error("Fail reaching rest api");
            this.LOG.error(e.getMessage(), e);
            throw new FlightAPIException(NestedExceptionUtils.getMostSpecificCause(e).getLocalizedMessage());
        }
        return responseEntity;
    }

    private void validateParameters(final Map<Object, Object> queryParams) throws FlightNotAcceptableException {
        for (int i = MANDATORY_PARAMS.size() - 1; i >= 0; i--) {
            if (StringUtils.isEmpty(queryParams.get(MANDATORY_PARAMS.get(i)))) {
                throw new FlightNotAcceptableException("Parameter " + MANDATORY_PARAMS.get(i) + " is missing");
            }
        }

    }

    private String buildURL(final Map<Object, Object> queryParams) {

        final StringBuilder url = new StringBuilder(BASE_URL);
        url.append("?");
        final StringBuilder params = new StringBuilder();
        String prefix = "";
        for (final Map.Entry<Object, Object> entry : queryParams.entrySet()) {
            params.append(prefix);
            prefix = "&";
            params.append(entry.getKey());
            params.append("=");
            params.append(entry.getValue());
        }
        url.append(params);

        return url.toString();
    }
}
