package flight.manager.challenge.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The type Flight.
 */
public class Flight {

    //    private int price;
    private String flyFrom;
    private String flyTo;
    private String cityFrom;
    private String cityTo;
    private ArrayList<String> airlines;
    private HashMap<Object, Object> bags_price;
    private float price;
    private HashMap conversion;
    private long dTimeUTC;
    private long aTimeUTC;

    /**
     * Instantiates a new Flight.
     */
    public Flight() {
        final var bags = new HashMap<>();
        bags.put("1", null);
        bags.put("2", null);
        this.bags_price = bags;

        final var priceCurrency = new HashMap<>();
        priceCurrency.put("GBP", null);
        priceCurrency.put("EUR", null);
        this.conversion = priceCurrency;
    }

    /**
     * Gets fly from.
     *
     * @return the fly from
     */
    public String getFlyFrom() {
        return this.flyFrom;
    }

    /**
     * Sets fly from.
     *
     * @param flyFrom the fly from
     */
    public void setFlyFrom(final String flyFrom) {
        this.flyFrom = flyFrom;
    }

    /**
     * Gets fly to.
     *
     * @return the fly to
     */
    public String getFlyTo() {
        return this.flyTo;
    }

    /**
     * Sets fly to.
     *
     * @param flyTo the fly to
     */
    public void setFlyTo(final String flyTo) {
        this.flyTo = flyTo;
    }

    /**
     * Gets city from.
     *
     * @return the city from
     */
    public String getCityFrom() {
        return this.cityFrom;
    }

    /**
     * Sets city from.
     *
     * @param cityFrom the city from
     */
    public void setCityFrom(final String cityFrom) {
        this.cityFrom = cityFrom;
    }

    /**
     * Gets city to.
     *
     * @return the city to
     */
    public String getCityTo() {
        return this.cityTo;
    }

    /**
     * Sets city to.
     *
     * @param cityTo the city to
     */
    public void setCityTo(final String cityTo) {
        this.cityTo = cityTo;
    }

    /**
     * Gets airlines.
     *
     * @return the airlines
     */
    public ArrayList<String> getAirlines() {
        return this.airlines;
    }

    /**
     * Sets airlines.
     *
     * @param airlines the airlines
     */
    public void setAirlines(final ArrayList<String> airlines) {
        this.airlines = airlines;
    }

    /**
     * Get bags price hash map .
     *
     * @return the hash map
     */
    public HashMap<Object, Object> getBags_price() {
        return this.bags_price;
    }

    /**
     * Sets bags price.
     *
     * @param bags_price the bags price
     */
    public void setBags_price(final HashMap bags_price) {
        this.bags_price = bags_price;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public float getPrice() {
        return this.price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(final float price) {
        this.price = price;
    }

    /**
     * Gets conversion.
     *
     * @return the conversion
     */
    public HashMap getConversion() {
        return this.conversion;
    }

    /**
     * Sets conversion.
     *
     * @param conversion the conversion
     */
    public void setConversion(final HashMap conversion) {
        this.conversion = conversion;
    }

    /**
     * Gets time utc.
     *
     * @return the time utc
     */
    public long getdTimeUTC() {
        return this.dTimeUTC;
    }

    /**
     * Sets time utc.
     *
     * @param dTimeUTC the d time utc
     */
    public void setdTimeUTC(final int dTimeUTC) {
        this.dTimeUTC = dTimeUTC;
    }

    /**
     * Gets time utc.
     *
     * @return the time utc
     */
    public long getaTimeUTC() {
        return this.aTimeUTC;
    }

    /**
     * Sets time utc.
     *
     * @param aTimeUTC the a time utc
     */
    public void setaTimeUTC(final int aTimeUTC) {
        this.aTimeUTC = aTimeUTC;
    }
}
