package flight.manager.challenge.utils;

public enum ErrorCodes {

    FLIGHT_NOT_FOUND("flight.not_found"),
    FLIGHT_NOT_ACCEPTABLE("flight.not_acceptable"),
    FLIGHT_API("skypicker.flight");

    private String code;

    ErrorCodes(String code) {
        this.code = code;
    }

    public String getResourceCode() {
        return code;
    }

    public void setResourceCode(String code) {
        this.code = code;
    }
}
