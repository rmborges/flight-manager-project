#using multi-stage build approach
#base image - build dependencies and generate jar
#building artifacts
FROM maven:3.6.3-jdk-13 AS build

ARG RELEASE_VERSION
ENV RELEASE_VERSION $RELEASE_VERSION

WORKDIR /app

COPY src /app/src
COPY pom.xml /app

RUN  mvn versions:set -DnewVersion=$RELEASE_VERSION
RUN mvn versions:update-child-modules -N
RUN mvn versions:commit
RUN mvn -f /app/pom.xml clean install

#######################################

#final image with overall size reduced by using the generated jar from base image
FROM openjdk:13-jdk-slim

ARG RELEASE_VERSION
ENV RELEASE_VERSION $RELEASE_VERSION

WORKDIR /app
COPY --from=build /app/target/challenge-$RELEASE_VERSION.jar /app/challenge-$RELEASE_VERSION.jar
EXPOSE 8080

CMD java -jar /app/challenge-$RELEASE_VERSION.jar